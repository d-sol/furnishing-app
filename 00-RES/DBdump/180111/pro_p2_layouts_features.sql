/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:49:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_p2_layouts_features
-- ----------------------------
DROP TABLE IF EXISTS `pro_p2_layouts_features`;
CREATE TABLE `pro_p2_layouts_features` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_p2_layouts_features
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
