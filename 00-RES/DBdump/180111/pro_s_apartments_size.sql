/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:50:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_s_apartments_size
-- ----------------------------
DROP TABLE IF EXISTS `pro_s_apartments_size`;
CREATE TABLE `pro_s_apartments_size` (
  `id` int(10) NOT NULL,
  `rooms` int(10) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `lower_border` int(10) DEFAULT NULL,
  `upper_border` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_s_apartments_size
-- ----------------------------
INSERT INTO `pro_s_apartments_size` VALUES ('1', '0', 'XS', '17', '22');
INSERT INTO `pro_s_apartments_size` VALUES ('2', '0', 'S', '22', '30');
INSERT INTO `pro_s_apartments_size` VALUES ('3', '0', 'M', '30', '40');
INSERT INTO `pro_s_apartments_size` VALUES ('4', '1', 'XS', '28', '34');
INSERT INTO `pro_s_apartments_size` VALUES ('5', '1', 'S', '34', '41');
INSERT INTO `pro_s_apartments_size` VALUES ('6', '1', 'M', '41', '52');
INSERT INTO `pro_s_apartments_size` VALUES ('7', '1', 'L', '52', '64');
INSERT INTO `pro_s_apartments_size` VALUES ('8', '1', 'XL', '64', '80');
INSERT INTO `pro_s_apartments_size` VALUES ('9', '1', 'XXL', '80', '200');
INSERT INTO `pro_s_apartments_size` VALUES ('10', '2', 'XS', '48', '55');
INSERT INTO `pro_s_apartments_size` VALUES ('11', '2', 'S', '55', '65');
INSERT INTO `pro_s_apartments_size` VALUES ('12', '2', 'M', '65', '74');
INSERT INTO `pro_s_apartments_size` VALUES ('13', '2', 'L', '74', '85');
INSERT INTO `pro_s_apartments_size` VALUES ('14', '2', 'XL', '85', '102');
INSERT INTO `pro_s_apartments_size` VALUES ('15', '2', 'XXL', '102', '200');
INSERT INTO `pro_s_apartments_size` VALUES ('16', '3', 'XS', '66', '74');
INSERT INTO `pro_s_apartments_size` VALUES ('17', '3', 'S', '74', '85');
INSERT INTO `pro_s_apartments_size` VALUES ('18', '3', 'M', '85', '95');
INSERT INTO `pro_s_apartments_size` VALUES ('19', '3', 'L', '95', '105');
INSERT INTO `pro_s_apartments_size` VALUES ('20', '3', 'XL', '105', '120');
INSERT INTO `pro_s_apartments_size` VALUES ('21', '3', 'XXL', '120', '200');
INSERT INTO `pro_s_apartments_size` VALUES ('22', '4', 'M', '100', '110');
INSERT INTO `pro_s_apartments_size` VALUES ('23', '4', 'L', '110', '123');
INSERT INTO `pro_s_apartments_size` VALUES ('24', '4', 'XL', '123', '153');
INSERT INTO `pro_s_apartments_size` VALUES ('25', '4', 'XXL', '153', '200');
SET FOREIGN_KEY_CHECKS=1;
