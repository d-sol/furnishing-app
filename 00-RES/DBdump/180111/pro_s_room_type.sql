/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:50:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_s_room_type
-- ----------------------------
DROP TABLE IF EXISTS `pro_s_room_type`;
CREATE TABLE `pro_s_room_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_ENG` varchar(255) DEFAULT NULL,
  `roomgroup_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_s_room_type
-- ----------------------------
INSERT INTO `pro_s_room_type` VALUES ('1', 'Balconies', '5');
INSERT INTO `pro_s_room_type` VALUES ('2', 'Bedrooms', '3');
INSERT INTO `pro_s_room_type` VALUES ('3', 'Bed & studies', '3');
INSERT INTO `pro_s_room_type` VALUES ('4', 'Camp sites', '5');
INSERT INTO `pro_s_room_type` VALUES ('5', 'Corridor sites', '1');
INSERT INTO `pro_s_room_type` VALUES ('6', 'Communal living rooms', '2');
INSERT INTO `pro_s_room_type` VALUES ('7', 'Concierge offices', '1');
INSERT INTO `pro_s_room_type` VALUES ('8', 'Conservatories', '5');
INSERT INTO `pro_s_room_type` VALUES ('9', 'Domestic dining rooms', '2');
INSERT INTO `pro_s_room_type` VALUES ('10', 'Dormitories', '2');
INSERT INTO `pro_s_room_type` VALUES ('11', 'Gardens', '5');
INSERT INTO `pro_s_room_type` VALUES ('12', 'Kitchen & Dining rooms', '2');
INSERT INTO `pro_s_room_type` VALUES ('13', 'Domestic kitchens', '2');
INSERT INTO `pro_s_room_type` VALUES ('14', 'Living rooms', '2');
INSERT INTO `pro_s_room_type` VALUES ('15', 'Nursing home bedrooms', '3');
INSERT INTO `pro_s_room_type` VALUES ('16', 'Panic rooms', '3');
INSERT INTO `pro_s_room_type` VALUES ('17', 'Showers', '4');
INSERT INTO `pro_s_room_type` VALUES ('18', 'Store rooms', '1');
INSERT INTO `pro_s_room_type` VALUES ('19', 'Sanitary', '4');
INSERT INTO `pro_s_room_type` VALUES ('20', 'Utility rooms', '4');
INSERT INTO `pro_s_room_type` VALUES ('21', 'Verandas', '5');
INSERT INTO `pro_s_room_type` VALUES ('22', 'Walk-in wardrobes', '3');
SET FOREIGN_KEY_CHECKS=1;
