/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:50:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_s_furniture_type
-- ----------------------------
DROP TABLE IF EXISTS `pro_s_furniture_type`;
CREATE TABLE `pro_s_furniture_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ENG` varchar(255) DEFAULT NULL,
  `xSize` double(10,0) unsigned DEFAULT NULL,
  `ySize` double(10,0) unsigned DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_s_furniture_type
-- ----------------------------
INSERT INTO `pro_s_furniture_type` VALUES ('1', 'Blender', '195', '178', null);
INSERT INTO `pro_s_furniture_type` VALUES ('2', 'Coffee Maker', '280', '370', null);
INSERT INTO `pro_s_furniture_type` VALUES ('3', 'Cooktop', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('4', 'Dryer', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('5', 'Hood', '890', '660', null);
INSERT INTO `pro_s_furniture_type` VALUES ('6', 'Icemaker', '450', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('7', 'Kettle BBQ', '300', '300', null);
INSERT INTO `pro_s_furniture_type` VALUES ('8', 'Microwave Oven', '550', '350', null);
INSERT INTO `pro_s_furniture_type` VALUES ('9', 'Oven Built-in', '600', '500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('10', 'Range', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('11', 'Refrigerator', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('12', 'Tea Cup', '115', '80', null);
INSERT INTO `pro_s_furniture_type` VALUES ('13', 'Tea Kettle', '310', '250', null);
INSERT INTO `pro_s_furniture_type` VALUES ('14', 'Trash Compactor', '450', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('15', 'Washer', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('16', 'Basin', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('17', 'Basin corner', '420', '420', null);
INSERT INTO `pro_s_furniture_type` VALUES ('18', 'Multibasin', '1600', '500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('19', 'Glass Pane', '900', '100', null);
INSERT INTO `pro_s_furniture_type` VALUES ('20', 'Grab Bar', '450', '80', null);
INSERT INTO `pro_s_furniture_type` VALUES ('21', 'Hand Dryer', '290', '170', null);
INSERT INTO `pro_s_furniture_type` VALUES ('22', 'Napkin Disposal', '330', '100', null);
INSERT INTO `pro_s_furniture_type` VALUES ('23', 'Safety Railing', '1200', '110', null);
INSERT INTO `pro_s_furniture_type` VALUES ('24', 'Seat Cover Dispenser', '380', '70', null);
INSERT INTO `pro_s_furniture_type` VALUES ('25', 'Shower Door', '900', '900', null);
INSERT INTO `pro_s_furniture_type` VALUES ('26', 'Soap Dispenser', '200', '100', null);
INSERT INTO `pro_s_furniture_type` VALUES ('27', 'Toilet Paper Dispenser', '160', '60', null);
INSERT INTO `pro_s_furniture_type` VALUES ('28', 'Towel Dispensers', '320', '100', null);
INSERT INTO `pro_s_furniture_type` VALUES ('29', 'Towel Rack', '800', '80', null);
INSERT INTO `pro_s_furniture_type` VALUES ('30', 'Waste Receptacle', '420', '150', null);
INSERT INTO `pro_s_furniture_type` VALUES ('31', 'Bathtub', '1700', '750', null);
INSERT INTO `pro_s_furniture_type` VALUES ('32', 'Bathtub Corner', '1500', '1500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('33', 'Jacuzzi', '1800', '1800', null);
INSERT INTO `pro_s_furniture_type` VALUES ('34', 'Shower Cabin', '800', '800', null);
INSERT INTO `pro_s_furniture_type` VALUES ('35', 'Bed', '900', '2000', null);
INSERT INTO `pro_s_furniture_type` VALUES ('36', 'Bed Double', '1800', '2000', null);
INSERT INTO `pro_s_furniture_type` VALUES ('37', 'Bookshelf, cabinet', '1200', '400', null);
INSERT INTO `pro_s_furniture_type` VALUES ('38', 'Wardrobe', '900', '500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('39', 'Armchair', '520', '520', null);
INSERT INTO `pro_s_furniture_type` VALUES ('40', 'Bar Stool', '400', '400', null);
INSERT INTO `pro_s_furniture_type` VALUES ('41', 'Bean Bag', '900', '900', null);
INSERT INTO `pro_s_furniture_type` VALUES ('42', 'Bench', '1500', '1500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('43', 'Chair', '400', '400', null);
INSERT INTO `pro_s_furniture_type` VALUES ('44', 'Designer Chair', '500', '530', null);
INSERT INTO `pro_s_furniture_type` VALUES ('45', 'Stool', '640', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('46', 'High Chair', '600', '625', null);
INSERT INTO `pro_s_furniture_type` VALUES ('47', 'Lounge Chair', '520', '1640', null);
INSERT INTO `pro_s_furniture_type` VALUES ('48', 'Office Chair', '830', '790', null);
INSERT INTO `pro_s_furniture_type` VALUES ('49', 'Sofa', '1600', '850', null);
INSERT INTO `pro_s_furniture_type` VALUES ('50', 'Carpet', '1400', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('51', 'Clock', '300', '15', null);
INSERT INTO `pro_s_furniture_type` VALUES ('52', 'Mirror', '500', '30', null);
INSERT INTO `pro_s_furniture_type` VALUES ('53', 'Picture', '500', '10', null);
INSERT INTO `pro_s_furniture_type` VALUES ('54', 'Text 3D', '4600', '300', null);
INSERT INTO `pro_s_furniture_type` VALUES ('55', 'Vase', '200', '200', null);
INSERT INTO `pro_s_furniture_type` VALUES ('56', 'Blind', '100', '50', null);
INSERT INTO `pro_s_furniture_type` VALUES ('57', 'Chair Layout', '2200', '2270', null);
INSERT INTO `pro_s_furniture_type` VALUES ('58', 'Bed Layout', '2800', '2000', null);
INSERT INTO `pro_s_furniture_type` VALUES ('59', 'Dining Table Rectangle', '2600', '1800', null);
INSERT INTO `pro_s_furniture_type` VALUES ('60', 'Dining Table Round', '2200', '2200', null);
INSERT INTO `pro_s_furniture_type` VALUES ('61', 'Kitchen Layout', '3000', '3000', null);
INSERT INTO `pro_s_furniture_type` VALUES ('62', 'Office Layout Island', '3000', '3000', null);
INSERT INTO `pro_s_furniture_type` VALUES ('63', 'Office Layout Linear', '3000', '1200', null);
INSERT INTO `pro_s_furniture_type` VALUES ('64', 'Office Workstation Solo', '1500', '1300', null);
INSERT INTO `pro_s_furniture_type` VALUES ('65', 'Sofa Layout', '4000', '2900', null);
INSERT INTO `pro_s_furniture_type` VALUES ('66', 'Tree', '5700', '5700', null);
INSERT INTO `pro_s_furniture_type` VALUES ('67', 'Bush', '1500', '1500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('68', 'Hedge', '2000', '500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('69', 'Parasol', '3500', '3500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('70', 'Soil Holder', '1200', '1200', null);
INSERT INTO `pro_s_furniture_type` VALUES ('71', 'Billiard Table', '1490', '2760', null);
INSERT INTO `pro_s_furniture_type` VALUES ('72', 'Book Cluster', '1000', '200', null);
INSERT INTO `pro_s_furniture_type` VALUES ('73', 'TV', '925', '50', null);
INSERT INTO `pro_s_furniture_type` VALUES ('74', 'Hi-fi Set', '500', '500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('75', 'Piano', '1500', '2500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('76', 'Piano-Upright', '1450', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('77', 'Sport Machine', '1800', '850', null);
INSERT INTO `pro_s_furniture_type` VALUES ('78', 'Table Tennis', '2755', '1525', null);
INSERT INTO `pro_s_furniture_type` VALUES ('79', 'Weight Machine', '1700', '850', null);
INSERT INTO `pro_s_furniture_type` VALUES ('80', 'Ceiling Lamp', '300', '300', null);
INSERT INTO `pro_s_furniture_type` VALUES ('81', 'Desk Lamp', '250', '400', null);
INSERT INTO `pro_s_furniture_type` VALUES ('82', 'Floor Lamp', '300', '250', null);
INSERT INTO `pro_s_furniture_type` VALUES ('83', 'Fluorescent Lamp', '1500', '300', null);
INSERT INTO `pro_s_furniture_type` VALUES ('84', 'Spotlamp Ceiling', '110', '150', null);
INSERT INTO `pro_s_furniture_type` VALUES ('85', 'Spotlight Kit', '500', '120', null);
INSERT INTO `pro_s_furniture_type` VALUES ('86', 'Cabinet Base Chamfered', '300', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('87', 'Cabinet Base Cooktop+Oven', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('88', 'Cabinet Base Corner C with Sink', '900', '900', null);
INSERT INTO `pro_s_furniture_type` VALUES ('89', 'Cabinet Base Corner L', '1200', '1200', null);
INSERT INTO `pro_s_furniture_type` VALUES ('90', 'Cabinet Base Dishwasher', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('91', 'Cabinet Base + Sink', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('92', 'Cabinet Base End', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('93', 'Cabinet Base MultiDrawer', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('94', 'Cabinet Tall +Oven', '900', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('95', 'Cabinet Wall Corner C', '600', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('96', 'Cabinet Wall Corner L', '900', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('97', 'Copier', '435', '650', null);
INSERT INTO `pro_s_furniture_type` VALUES ('98', 'Flipchart', '780', '700', null);
INSERT INTO `pro_s_furniture_type` VALUES ('99', 'Monitor', '490', '120', null);
INSERT INTO `pro_s_furniture_type` VALUES ('100', 'Keyboard', '430', '115', null);
INSERT INTO `pro_s_furniture_type` VALUES ('101', 'Laptop', '330', '245', null);
INSERT INTO `pro_s_furniture_type` VALUES ('102', 'Mac mini', '165', '165', null);
INSERT INTO `pro_s_furniture_type` VALUES ('103', 'PC', '180', '400', null);
INSERT INTO `pro_s_furniture_type` VALUES ('104', 'Mouse', '60', '100', null);
INSERT INTO `pro_s_furniture_type` VALUES ('105', 'Office Partition Panel', '800', '80', null);
INSERT INTO `pro_s_furniture_type` VALUES ('106', 'Projector', '310', '265', null);
INSERT INTO `pro_s_furniture_type` VALUES ('107', 'Telephone', '210', '250', null);
INSERT INTO `pro_s_furniture_type` VALUES ('108', 'Rectangle table', '1200', '600', null);
INSERT INTO `pro_s_furniture_type` VALUES ('109', 'Circle Table', '1000', '1000', null);
INSERT INTO `pro_s_furniture_type` VALUES ('110', 'Office Desk L-Shaped', '1500', '1300', null);
INSERT INTO `pro_s_furniture_type` VALUES ('111', 'Office Table Round', '1200', '1200', null);
INSERT INTO `pro_s_furniture_type` VALUES ('112', 'Bidet', '370', '500', null);
INSERT INTO `pro_s_furniture_type` VALUES ('113', 'Urinal', '330', '330', null);
INSERT INTO `pro_s_furniture_type` VALUES ('114', 'WC', '350', '640', null);
INSERT INTO `pro_s_furniture_type` VALUES ('115', 'Dining Table', null, null, null);
INSERT INTO `pro_s_furniture_type` VALUES ('116', 'FreeZone', null, null, null);
SET FOREIGN_KEY_CHECKS=1;
