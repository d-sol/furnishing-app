/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:50:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_s_hyperobjects
-- ----------------------------
DROP TABLE IF EXISTS `pro_s_hyperobjects`;
CREATE TABLE `pro_s_hyperobjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ENG` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_s_hyperobjects
-- ----------------------------
INSERT INTO `pro_s_hyperobjects` VALUES ('1', 'PROhyperobject_Appliances');
INSERT INTO `pro_s_hyperobjects` VALUES ('2', 'PROhyperobject_Basins');
INSERT INTO `pro_s_hyperobjects` VALUES ('3', 'PROhyperobject_Bathroom Accessories');
INSERT INTO `pro_s_hyperobjects` VALUES ('4', 'PROhyperobject_Bathtubs, Showers, Jacuzzis');
INSERT INTO `pro_s_hyperobjects` VALUES ('5', 'PROhyperobject_Beds');
INSERT INTO `pro_s_hyperobjects` VALUES ('6', 'PROhyperobject_Cabinets and Shelves');
INSERT INTO `pro_s_hyperobjects` VALUES ('7', 'PROhyperobject_Chairs');
INSERT INTO `pro_s_hyperobjects` VALUES ('8', 'PROhyperobject_Couches and Sofas');
INSERT INTO `pro_s_hyperobjects` VALUES ('9', 'PROhyperobject_Decoration');
INSERT INTO `pro_s_hyperobjects` VALUES ('10', 'PROhyperobject_Furniture Layouts');
INSERT INTO `pro_s_hyperobjects` VALUES ('11', 'PROhyperobject_Garden');
INSERT INTO `pro_s_hyperobjects` VALUES ('12', 'PROhyperobject_Health and Recreation');
INSERT INTO `pro_s_hyperobjects` VALUES ('13', 'PROhyperobject_Interior Lamps');
INSERT INTO `pro_s_hyperobjects` VALUES ('14', 'PROhyperobject_Kitchen Cabinets');
INSERT INTO `pro_s_hyperobjects` VALUES ('15', 'PROhyperobject_Office Equipment');
INSERT INTO `pro_s_hyperobjects` VALUES ('16', 'PROhyperobject_Sinks');
INSERT INTO `pro_s_hyperobjects` VALUES ('17', 'PROhyperobject_Tables');
INSERT INTO `pro_s_hyperobjects` VALUES ('18', 'PROhyperobject_WC, Bidets, Urinals');
SET FOREIGN_KEY_CHECKS=1;
