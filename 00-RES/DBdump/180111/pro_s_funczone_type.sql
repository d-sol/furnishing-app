/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:50:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_s_funczone_type
-- ----------------------------
DROP TABLE IF EXISTS `pro_s_funczone_type`;
CREATE TABLE `pro_s_funczone_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_ENG` varchar(255) DEFAULT NULL,
  `name_RUS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_s_funczone_type
-- ----------------------------
INSERT INTO `pro_s_funczone_type` VALUES ('1', 'Halloway Zone', 'Входная зона');
INSERT INTO `pro_s_funczone_type` VALUES ('2', 'Wardrobe Zone', 'Гардеробная зона');
INSERT INTO `pro_s_funczone_type` VALUES ('3', 'Kitchen Zone', 'Кухонная зона');
INSERT INTO `pro_s_funczone_type` VALUES ('4', 'Dining Zone', 'Обеденная зона');
INSERT INTO `pro_s_funczone_type` VALUES ('5', 'Public Zone', 'Публичная зона');
INSERT INTO `pro_s_funczone_type` VALUES ('6', 'Desktop Zone', 'Рабочая зона');
INSERT INTO `pro_s_funczone_type` VALUES ('7', 'Sanitary Zone', 'Санитарная зона');
INSERT INTO `pro_s_funczone_type` VALUES ('8', 'Utilitarian Zone', 'Утилитарная зона');
INSERT INTO `pro_s_funczone_type` VALUES ('9', 'Sleep Zone', 'Зона сна');
INSERT INTO `pro_s_funczone_type` VALUES ('10', 'Free Zone', 'Свободная зона');
INSERT INTO `pro_s_funczone_type` VALUES ('11', 'Open Air Zone', 'Зона летних помещений');
SET FOREIGN_KEY_CHECKS=1;
