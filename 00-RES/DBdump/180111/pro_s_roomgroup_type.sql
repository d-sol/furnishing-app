/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:50:50
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_s_roomgroup_type
-- ----------------------------
DROP TABLE IF EXISTS `pro_s_roomgroup_type`;
CREATE TABLE `pro_s_roomgroup_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_ENG` varchar(255) DEFAULT NULL,
  `name_RUS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_s_roomgroup_type
-- ----------------------------
INSERT INTO `pro_s_roomgroup_type` VALUES ('1', 'Entrance group', 'Входная группа');
INSERT INTO `pro_s_roomgroup_type` VALUES ('2', 'KitchenDiningLiving group', 'Группа кухни-столовой-гостинной');
INSERT INTO `pro_s_roomgroup_type` VALUES ('3', 'Bedroom group', 'Группа спальных помещений');
INSERT INTO `pro_s_roomgroup_type` VALUES ('4', 'Domestic group', 'Группа бытовых помещений');
INSERT INTO `pro_s_roomgroup_type` VALUES ('5', 'Openair group', 'Группа летних помещений');
SET FOREIGN_KEY_CHECKS=1;
