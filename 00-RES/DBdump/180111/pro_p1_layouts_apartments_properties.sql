/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:49:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_p1_layouts_apartments_properties
-- ----------------------------
DROP TABLE IF EXISTS `pro_p1_layouts_apartments_properties`;
CREATE TABLE `pro_p1_layouts_apartments_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `layout_id` varchar(255) DEFAULT NULL,
  `features_id` int(10) DEFAULT NULL,
  `features_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_p1_layouts_apartments_properties
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
