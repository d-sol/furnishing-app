/*
Navicat MySQL Data Transfer

Source Server         : PROtest
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : portal_protest

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2019-01-11 16:50:19
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for pro_s_furniture_actodb
-- ----------------------------
DROP TABLE IF EXISTS `pro_s_furniture_actodb`;
CREATE TABLE `pro_s_furniture_actodb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ArchiCAD_furniture_name` varchar(255) DEFAULT NULL,
  `DB_furniture_id` int(10) DEFAULT NULL,
  `is_static` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=287 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pro_s_furniture_actodb
-- ----------------------------
INSERT INTO `pro_s_furniture_actodb` VALUES ('1', 'Blender', '1', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('2', 'Coffee Maker', '2', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('3', 'Cooktop Built-in', '3', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('4', 'Cooktop Electric', '3', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('5', 'Cooktop Gas', '3', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('6', 'Dryer', '4', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('7', 'Hood', '5', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('8', 'Hood Built-in', '5', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('9', 'Icemaker', '6', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('10', 'Kettle BBQ', '7', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('11', 'Microwave Oven', '8', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('12', 'Oven Built-in', '9', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('13', 'Range Electric', '10', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('14', 'Range Gas', '10', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('15', 'Refrigerator', '11', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('16', 'Refrigerator Side by Side', '11', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('17', 'Tea Cup', '12', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('18', 'Tea Kettle', '13', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('19', 'Trash Compactor', '14', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('20', 'Washer', '15', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('21', 'Basin', '16', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('22', 'Basin Cabinet', '16', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('23', 'Basin Corner', '17', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('24', 'Basin Double', '18', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('25', 'Multi-Basin Counter', '18', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('26', 'Glass Pane', '19', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('27', 'Grab Bar', '20', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('28', 'Hand Dryer', '21', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('29', 'Napkin Disposal', '22', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('30', 'Safety Railing', '23', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('31', 'Seat Cover Dispenser', '24', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('32', 'Shower Door', '25', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('33', 'Soap Dispenser', '26', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('34', 'Toilet Paper Dispenser', '27', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('35', 'Towel Dispensers', '28', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('36', 'Towel Rack', '29', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('37', 'Waste Receptacle', '30', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('38', 'Bathtub', '31', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('39', 'Bathtub Corner', '32', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('40', 'Freestanding Bathtub', '31', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('41', 'Jacuzzi', '33', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('42', 'Shower Cabin', '34', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('43', 'Shower Tray', '34', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('44', 'Bed', '35', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('45', 'Bed Bunk', '35', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('46', 'Bed Canopy', '36', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('47', 'Bed Double', '36', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('48', 'Crib', '35', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('49', 'Night Table', '37', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('50', 'Bookshelf 01', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('51', 'Bookshelf 02', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('52', 'Built-in Wardrobe', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('53', 'Credenza', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('54', 'Cupboard', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('55', 'Flexible Shelf', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('56', 'Lockers', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('57', 'Office Cabinet', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('58', 'Overhead Cabinet', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('59', 'Plan Drawers', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('60', 'Underdesk Drawer', '37', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('61', 'Wall Cabinet', '37', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('62', 'Wardrobe', '38', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('63', 'Wardrobe Modular', '38', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('64', 'Wardrobe Variable', '38', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('65', 'Armchair 01', '39', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('66', 'Armchair 02', '39', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('67', 'Armchair 03', '39', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('68', 'Armchair 04', '39', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('69', 'Bar Stool', '40', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('70', 'Bean Bag', '41', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('71', 'Bench', '42', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('72', 'Chair 01', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('73', 'Chair 02', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('74', 'Chair 03', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('75', 'Chair 04', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('76', 'Chair 05', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('77', 'Chair 06', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('78', 'Chair 07', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('79', 'Chair 08', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('80', 'Chair 09', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('81', 'Chair 10', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('82', 'Designer Chair 01', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('83', 'Designer Chair 02', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('84', 'Designer Chair 03', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('85', 'Designer Chair 04', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('86', 'Designer Chair 05', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('87', 'Designer Chair 06', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('88', 'Designer Chair 07', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('89', 'Designer Chair 08', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('90', 'Designer Chair 09', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('91', 'Designer Chair 10', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('92', 'Designer Chair 11', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('93', 'Designer Chair 12', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('94', 'Designer Chair 13', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('95', 'Designer Chair 14', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('96', 'Designer Stool', '45', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('97', 'Folding Chair', '43', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('98', 'High Chair 01', '46', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('99', 'High Chair 02', '46', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('100', 'High Chair 03', '46', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('101', 'Lounge Chair', '47', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('102', 'Office Chair 01', '48', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('103', 'Office Chair 02', '48', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('104', 'Office Chair 03', '48', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('105', 'Office Chair 04', '48', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('106', 'Office Chair 05', '48', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('107', 'Office Chair 06', '48', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('108', 'Ottoman', '41', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('109', 'Overstuffed Chair', '44', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('110', 'Piano Bench', '45', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('111', 'Piano Stool', '40', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('112', 'Rocking Chair', '47', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('113', 'Stool', '45', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('114', 'Bamboo Couch', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('115', 'Bamboo Seat', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('116', 'Designer Couch', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('117', 'Designer Sofa 01', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('118', 'Designer Sofa 02', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('119', 'Designer Sofa 03', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('120', 'Sofa', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('121', 'Sofa Bed', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('122', 'Sofa Set 01', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('123', 'Sofa Set 02', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('124', 'Wicker Chair', '49', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('125', 'Wicker Couch', '49', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('126', 'Carpet', '50', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('127', 'Clock', '51', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('128', 'Mirror', '52', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('129', 'Picture', '53', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('130', 'Text 3D', '54', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('131', 'Vase', '55', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('132', 'Venetian Blind', '56', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('133', 'Vertical Blinds', '56', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('134', 'Audience Seating', '57', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('135', 'Bed Layout', '58', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('136', 'Chair Layout', '57', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('137', 'Dining Table Rectangle', '59', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('138', 'Dining Table Round', '60', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('139', 'Kitchen Layout', '61', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('140', 'Office Layout Island', '62', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('141', 'Office Layout Linear', '63', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('142', 'Office Workstation Solo', '64', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('143', 'Sofa Layout', '65', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('144', 'Deciduous Trees', '66', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('145', 'Evergreens', '66', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('146', 'Grass Model', '67', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('147', 'Hedge Model', '68', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('148', 'Houseplant Model', '67', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('149', 'Houseplants', '67', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('150', 'Palm Tree Model', '66', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('151', 'Palm Trees', '66', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('152', 'Parasol', '69', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('153', 'Pine Tree Model', '66', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('154', 'Pine Trees', '66', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('155', 'Shrubs', '67', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('156', 'Soil Holder', '70', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('157', 'Tree Model Detailed', '66', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('158', 'Tree Model Simple 1', '66', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('159', 'Tree Model Simple 2', '66', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('160', 'Billiard Table', '71', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('161', 'Book Cluster', '72', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('162', 'Flat Panel TV', '73', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('163', 'Hi-fi Set', '74', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('164', 'Piano', '75', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('165', 'Piano-Upright', '76', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('166', 'Rowing Machine', '77', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('167', 'Stairmaster', '77', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('168', 'Stationary Bicycle', '77', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('169', 'Table Tennis', '78', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('170', 'Treadmill', '77', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('171', 'TV', '73', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('172', 'Weight Bench', '79', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('173', 'Weight Machine', '79', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('174', 'Ceiling Lamp', '80', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('175', 'Ceiling Ventillator', '80', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('176', 'Chandelier', '80', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('177', 'Desk Lamp 01', '81', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('178', 'Desk Lamp 02', '81', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('179', 'Desk Lamp 03', '81', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('180', 'Desk Lamp 04', '81', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('181', 'Desk Lamp 05', '81', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('182', 'Floor Lamp 01', '82', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('183', 'Floor Lamp 02', '82', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('184', 'Floor Lamp 03', '82', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('185', 'Floor Lamp 04', '82', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('186', 'Fluorescent Lamp', '83', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('187', 'Fluorescent Wall Lamp', '83', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('188', 'Halogen Recessed', '80', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('189', 'Pendant Lamp', '80', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('190', 'Recessed Spot', '80', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('191', 'Spotlamp Ceiling', '84', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('192', 'Spotlamp Ground', '84', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('193', 'Spotlamp Wall', '84', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('194', 'Spotlight Kit', '85', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('195', 'Wall Lamp', '84', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('196', 'Wall Spotlight', '85', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('197', 'Cabinet Base Chamfered', '86', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('198', 'Cabinet Base Cooktop+Oven', '87', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('199', 'Cabinet Base Corner C with Sink', '88', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('200', 'Cabinet Base Corner L', '89', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('201', 'Cabinet Base Corner S', '89', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('202', 'Cabinet Base Dishwasher', '90', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('203', 'Cabinet Base Double Door', '91', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('204', 'Cabinet Base End', '92', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('205', 'Cabinet Base MultiDrawer', '93', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('206', 'Cabinet Base Single Door', '91', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('207', 'Cabinet Base Triple Door', '91', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('208', 'Cabinet Belfast Double Door with Sink', '91', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('209', 'Cabinet Belfast Single Door with Sink', '91', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('210', 'Cabinet Tall Double Door', '93', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('211', 'Cabinet Tall Double Door with Oven', '94', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('212', 'Cabinet Tall Single Door', '93', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('213', 'Cabinet Tall Single Door with Oven', '94', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('214', 'Cabinet Wall Corner C', '95', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('215', 'Cabinet Wall Corner L', '96', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('216', 'Cabinet Wall Corner S', '96', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('217', 'Cabinet Wall Double Door', '93', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('218', 'Cabinet Wall End', '92', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('219', 'Cabinet Wall Single Door', '93', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('220', 'Cabinet Wall Triple Door', '93', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('221', 'Kitchen Counter Drawer', '93', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('222', 'Shelf Base Chamfered', '86', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('223', 'Shelf Base End 01', '92', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('224', 'Shelf Base End 02', '92', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('225', 'Shelf Wall', '93', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('226', 'Shelf Wall Chamfered', '86', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('227', 'Shelf Wall End', '92', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('228', 'Copier', '97', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('229', 'Copier Desktop', '97', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('230', 'Flipchart', '98', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('231', 'iMac', '99', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('232', 'Keyboard (Apple)', '100', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('233', 'Keyboard', '100', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('234', 'Laptop', '101', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('235', 'Mac mini', '102', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('236', 'Mac Pro', '103', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('237', 'MacBook', '101', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('238', 'Monitor (LCD)', '99', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('239', 'Monitor', '99', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('240', 'Mouse (Apple)', '104', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('241', 'Office Partition Panel', '105', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('242', 'PC', '103', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('243', 'Plotter', '97', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('244', 'Projector', '106', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('245', 'Projector Screen', '98', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('246', 'Telephone', '107', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('247', 'Mop Sink', '18', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('248', 'Sink Belfast', '18', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('249', 'Sink Corner', '88', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('250', 'Sink General', '18', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('251', 'Bamboo Table', '108', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('252', 'Coffee Table 01', '109', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('253', 'Coffee Table 02', '108', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('254', 'Computer Table', '64', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('255', 'Designer Table 01', '108', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('256', 'Designer Table 02', '109', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('257', 'Designer Table 03', '108', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('258', 'Designer Table 04', '108', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('259', 'Designer Table 05', '109', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('260', 'Desk', '108', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('261', 'Desk Extensions', '109', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('262', 'Dining Table 01', '115', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('263', 'Dining Table 02', '115', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('264', 'Dining Table 03', '115', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('265', 'Dining Table 04', '115', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('266', 'Drafting Table', '108', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('267', 'Lobby Table', '108', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('268', 'Office Desk L-Shaped', '110', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('269', 'Office Desk Rect', '64', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('270', 'Office Table', '64', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('271', 'Office Table Round', '111', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('272', 'Rectangular Table 01', '108', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('273', 'Rectangular Table 02', '108', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('274', 'Round Table 01', '109', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('275', 'Round Table 02', '109', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('276', 'Round Table 03', '109', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('277', 'Wall Mount Table', '108', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('278', 'Wicker Table', '108', '0');
INSERT INTO `pro_s_furniture_actodb` VALUES ('279', 'Bidet', '112', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('280', 'Squatting Toilet', '114', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('281', 'Urinal', '113', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('282', 'Urinal Corner', '113', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('283', 'WC', '114', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('284', 'WC Corner', '114', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('285', 'WC Disabled', '114', '1');
INSERT INTO `pro_s_furniture_actodb` VALUES ('286', 'FreeZone', '116', '0');
SET FOREIGN_KEY_CHECKS=1;
