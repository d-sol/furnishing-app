import generated.tables.records.ProC3FurnituresRecord;
import javax.swing.*;
import java.awt.*;
import java.util.Iterator;
import java.util.Map;

public class Canvas extends JFrame{
    double width;
    double height;
    Map<Integer, ProC3FurnituresRecord> arrang;

    Canvas (Polygon room, Map<Integer, ProC3FurnituresRecord> arrangement) {

        super("Furniture arrangement, "+ room.sides.get(1)+"x"+room.sides.get(0));
        this.arrang=arrangement;
        this.width=room.sides.get(1)*0.5;
        this.height=room.sides.get(0)*0.5;
       /*
       for (ProC3FurnituresRecord rec: arrangement.values()) {
            System.out.println(rec.getXcoord());
            System.out.println(rec.getYsize());
        }
        */



        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setSize((int)width+50,(int)height+50);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
       // System.out.println("Frame has width ="+this.width+" Frame has height ="+this.height);

    }

    void draw() {

        this.setVisible(true);

            Draw object = new Draw((int) this.height, (int)this.width, arrang);
            this.add(object);
            object.drawing();
        }


}
