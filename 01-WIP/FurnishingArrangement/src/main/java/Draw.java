import generated.tables.records.ProC3FurnituresRecord;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Map;

public class Draw extends JPanel {
    Map<Integer, ProC3FurnituresRecord> furniture;
    private int maxY;
    private int maxX;
    private double xCoord;
    private double yCoord;
    private double xSize;
    private double ySize;

    Draw(int height, int width, Map<Integer, ProC3FurnituresRecord> arrang){
       this.maxY=height;
        this.maxX=width;
       this.furniture=arrang;
       this.setBounds(0,0,maxX,maxY);
       this.setBorder(BorderFactory.createLineBorder(Color.black));
    }

    public void drawing() {
        repaint();
    }


    public void paintComponent(Graphics g) {
        this.setBackground( new Color(220, 233, 237) );
        super.paintComponent(g);
        g.drawRect(0,0,maxX,maxY);
        g.setColor(new Color(62, 62, 62));
        for (ProC3FurnituresRecord entry : furniture.values()) {
            Rectangle2D rect = new Rectangle2D.Double(entry.getXcoord()/2, this.maxY - entry.getYcoord()/2-entry.getYsize()/2, entry.getXsize()/2, entry.getYsize()/2);
            g.drawString("object_id: "+entry.getObjectId(),entry.getXcoord().intValue()/2+1,maxY-entry.getYcoord().intValue()/2-entry.getYsize().intValue()/4);
            Graphics2D g2 = (Graphics2D) g;
            g2.draw(rect);
        }

    }
}