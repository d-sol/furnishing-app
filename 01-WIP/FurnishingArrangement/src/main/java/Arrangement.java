import com.zaxxer.hikari.HikariDataSource;
import generated.tables.records.ProC2RoomsRecord;
import generated.tables.records.ProC3FurnituresRecord;
import org.jooq.*;
import org.jooq.impl.DSL;
import java.sql.Connection;
import java.util.*;

import static generated.Tables.*;

public class Arrangement {

    //connections
    static private HikariDataSource ds;
    static private DSLContext db;

    private Polygon room;

    private HashMap<Integer, Polygon> candidates = new HashMap<>(); //candidates for sampleRoom

    private Polygon sampleRoom;
    private Map<Integer, ProC3FurnituresRecord> sampleArrangement;

    private Map<Integer, ProC3FurnituresRecord> arrangement;

    static {
        //connection settings
        String userName = "d.soloviova";
        String password = "GJSqWCCyRtAh3V4k";
        String url = "jdbc:mysql://portal.archimatika.com:3306/dev.pro_apartments?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        ds = new HikariDataSource();
        ds.setJdbcUrl(url);
        ds.setUsername(userName);
        ds.setPassword(password);
    }

    Arrangement(double[] arr, int roomType) {
        room = new Polygon(arr);
        try (Connection conn = ds.getConnection()){
            db = DSL.using(conn, SQLDialect.MYSQL);
            initCandidates(roomType);
            initSamples();
            arrange();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    private void initCandidates(int roomType) {
        //TODO: skip non-furnished rooms
        int testId = 1706; // id of room from db to test algorithm
        //execute sql to get blobs of certain room type in convertable format, not too safe because of plain sql
        String sql = "SELECT id, ST_AsText(polygon) as polygon\n" +
                "FROM `dev.pro_apartments`.pro_c2_rooms\n" +
                "WHERE pro_c2_rooms.type_id = " + roomType + ";"; //type_id of bedroom

        //create id - polygon  map
        int pointsNum = room.points.size();
        db.fetch(sql).intoMap(PRO_C2_ROOMS.ID, PRO_C2_ROOMS.POLYGON).forEach(
                (k, v) -> {
                    if (Polygon.pointsOfBlob(v.toString()).length == pointsNum && k.intValue() != testId)
                        candidates.put(k.intValue(), new Polygon(v.toString()));
                });
    }

    private void initSamples() {
        int sampleRoomId = 0;
        double maxSimilarity = 0;

        //now working only on 4 points
        for (Map.Entry<Integer, Polygon> e : candidates.entrySet()) {
            if (similarityTo(e.getValue()) > maxSimilarity)
                sampleRoomId = e.getKey();
        }

        System.out.println("Sample room ID: "+sampleRoomId);
        //getting sample room record by id
        //maybe create record - polygon map of candidates to omit all this??
        ProC2RoomsRecord sampleRoom = db.fetchOne(PRO_C2_ROOMS, PRO_C2_ROOMS.ID.like(sampleRoomId + ""));
        //getting furniture records of sample room, probably very bugged
        sampleArrangement =
                ((ForeignKey<ProC3FurnituresRecord, Record>)PRO_C3_FURNITURES.getReferences().get(0))
                        .fetchChildren(sampleRoom)
                        .intoMap(PRO_C3_FURNITURES.ID);
    }

    double similarityTo(Polygon sample) {
        double sidesRelDiff = (sample.sides.get(0)/sample.sides.get(1)) /
                (room.sides.get(0)/room.sides.get(1));
        double areasDiff = sample.area / room.area;
        double perimDiff = sample.perimeter / room.perimeter;

      /*  System.out.println(sidesRelDiff);
        System.out.println(areasDiff);
        System.out.println(perimDiff);
        System.out.println("------------------------------------------------------------------------------");*/

        float a = 0.4f, b = 0.3f, c = 0.3f;
        return a * sidesRelDiff + b * areasDiff + c * perimDiff;
    }

    private void arrange() {
        //TODO: arrange furniture
    }

    public Polygon getRoom() {
        return room;
    }

    public Map<Integer, ProC3FurnituresRecord> getArrangement(){
        return arrangement;
    }

    public Polygon getSampleRoom() {
        return sampleRoom;
    }

    public Map<Integer, ProC3FurnituresRecord> getSampleArrangement() {
        return sampleArrangement;
    }
}

