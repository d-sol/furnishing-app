import javafx.geometry.Point2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Polygon {
    ArrayList<Point2D> points = new ArrayList<>();
    ArrayList<Double> sides = new ArrayList<>();
    double area;
    double perimeter;

    static String[] pointsOfBlob(String blob) {
        return blob.replaceAll("[^0-9 .,]+", "").split(",");
    }

    //converting kwt string to polygon
    Polygon(String blob) {
        String[] tmPoint;
        Point2D prevPoint;
        Point2D curPoint;
        for (String s : pointsOfBlob(blob)) {
           tmPoint = s.split(" ");
           points.add(new Point2D(Double.valueOf(tmPoint[0]), Double.valueOf(tmPoint[1])));
        }

        //TODO: reduce duplicate
        prevPoint = points.get(0);
        for (int i = 1; i < points.size(); i++) {
            curPoint = points.get(i);
            sides.add(prevPoint.distance(curPoint));
            prevPoint = curPoint;
        }
        perimeter = sides.stream().reduce((a, b) -> a + b).orElse(0.0);
        //TODO: area calc should depend on sides number
        area = sides.get(0) * sides.get(1);
    }

    Polygon(double[] arr) {
        this(new ArrayList<>(
                Arrays.stream(arr)
                        .boxed()
                        .collect(Collectors.toList())));
    }

    Polygon(ArrayList<Double> pointsArr) {
        for (int i = 0; i < pointsArr.size(); i += 2) {
            points.add(new Point2D(pointsArr.get(i), pointsArr.get(i + 1)));
        }
        Point2D prevPoint;
        Point2D curPoint;

        //duplicate
        prevPoint = points.get(0);
        for (int i = 1; i < points.size(); i++) {
            curPoint = points.get(i);
            sides.add(prevPoint.distance(curPoint));
            prevPoint = curPoint;
        }
        perimeter = sides.stream().reduce((a, b) -> a + b).orElse(0.0);
        //TODO: area calc should depend on sides number
        area = sides.get(0) * sides.get(1);
    }


    @Override
    public String toString() {
        return "Polygon of " + sides.size() + " sides";
    }
}

