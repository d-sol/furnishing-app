import generated.tables.records.ProC2RoomsRecord;
import generated.tables.records.ProC3FurnituresRecord;
import org.jooq.ForeignKey;
import org.jooq.Record;
import org.junit.Assert;
import org.junit.Test;

import static generated.Tables.PRO_C2_ROOMS;
import static generated.Tables.PRO_C3_FURNITURES;
import static org.junit.Assert.*;

public class ArrangementTest {
    @Test
    public void testAdd() {
        String str = "Junit is working fine";
        System.out.println("test");
        assertEquals("Junit is working fine", str);
    }

    @Test
    public void createArrangementOfBedroom(){
        double[] arr = { 0, 1200, 0, 0, 2610, 0, 2610, 1200, 0, 1200};
        new Arrangement(arr, 2);
    }


    // i had suddenly understood that no one except me would write this, so i just decided to test something
    // testing similarity of two polygons

    @Test
    public void quadrangleSimilarityToItselfShouldBe1() {
        double[] arr = { 0, 1200, 0, 0, 2610, 0, 2610, 1200, 0, 1200};
        assertEquals(1.0, new Arrangement(arr, 2).similarityTo(new Polygon(arr)), 0.001);
    }

    //TODO: same tests (shouldBe1) for all types of polygons

    @Test
    public void similarityToClosestShouldBeHigh(){
        double[] arr = { 0, 1200, 0, 0, 2610, 0, 2610, 1200, 0, 1200};
        double[] arr2 = {0, 1100, 0, 0, 2600, 0, 2600, 1100, 0, 1100};
        assertTrue(new Arrangement(arr, 2).similarityTo(new Polygon(arr2)) > 0.8);
    }


}
